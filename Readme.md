The folder Ipopt-3.12.12 contains Ipopt version 3.12.12 as downloaded on January 30, 2019 from https://www.coin-or.org/download/source/Ipopt/
The only additions the CMake files in the present folder and the test folder.
For license information, please refer to the files in the Ipopt-3.12.12 folder.
